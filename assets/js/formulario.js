const handleSubmit = (e) => {
  e.preventDefault();
  let myForm = document.getElementById("contactMsg");
  let formData = new FormData(myForm);
  fetch("/", {
    method: "POST",
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    body: new URLSearchParams(formData).toString(),
  })
    .then(() => document.querySelector("#pnlResult").innerHTML = "Mensaje enviado"
    ).then(
        //console.log("ocultar panel")
        setTimeout(() => {
            document.querySelector("#pnlResult").style.display = "none";
        }, 5000)
    )
    .catch((error) => alert(error));
};

document.querySelector("form").addEventListener("submit", handleSubmit);
